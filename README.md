CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Notes
 * Maintainers


INTRODUCTION
------------

The Bootstrap External Link Pop-up module extends the External Link Pop-up
module and replaces the jquery.UI popup dialog with Bootstrap's modal dialog.
This module is helpful if your frontend theme is built using the Bootstrap
framework.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/bootstrap_external_link_popup


REQUIREMENTS
------------

 * This module requires External link Pop-up, visit the project page:
   https://www.drupal.org/project/external_link_popup



INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Follow the configuration documented by the External Link Pop-up module.
 * Make sure your frontend theme is uses Bootstrap and loads the javscript
   libraries.


NOTES
-----

This module does not provide the Bootstrap styling or JavaScript libraries. You
must be using a theme that provides these. For example: Radix, Barrio, or your
own custom theme.


MAINTAINERS
-----------

 * Chris Snyder (chrissnyder)- https://www.drupal.org/u/chrissnyder
